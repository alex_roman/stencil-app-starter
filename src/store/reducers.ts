// import myReducer from './myReducer';

import { combineReducers } from 'redux';

export interface ApplicationState {
  layout: LayoutState
}


const rootReducer = (combineReducers as any)({
  // myReducer
});

export default rootReducer;