import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './reducers'


const sagaMiddleware = createSagaMiddleware()
const configureStore = (preloadedState: any) =>
  createStore(rootReducer, preloadedState, applyMiddleware(sagaMiddleware))

export { configureStore };