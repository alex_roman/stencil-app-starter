import { Component, Prop } from '@stencil/core';
import { configureStore } from './store';
import { Store } from '@stencil/redux';

@Component({
  tag: 'app-root',
  shadow: true
})
export class AppRoot {
  @Prop({ context: 'store' }) store: Store

  componentWillLoad() {
    this.store.setStore(configureStore({}))
  }

  render() {
    return (
      <div>
        hai salutare
      </div>
    );
  }
}
